package com.example.frontend_java.controller.granjita;

import com.example.frontend_java.controller.granjita.commands.ShellCommandHolder;
import com.example.frontend_java.controller.granjita.commands.diff_equations.ExactEqSolver;
import com.example.frontend_java.controller.granjita.commands.diff_equations.ExactEqValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

class ConsoleTest {
    static Console console;

    @BeforeAll
    static void initConsole(){
        console = new Console();
    }

    @Test
    void basicTest(){
         ShellCommandHolder holder = new ShellCommandHolder();
         holder.strip("python3 hello.py");
        console.runProcess(holder.getArguments());
        assertEquals("Hello World", console.getStdOut());
    }

    @Test
    void useSolveCommand(){
        Executable solver = new ExactEqSolver();
        console.runProcess(solver.getCommand("3*x**2 -2*x -y = -2*y + x - 3*y**2"));
        if (console.hasErrors()){
            System.out.println(console.getStdErr());
            fail();
        }else {
            System.out.println(console.getStdOut());
        }
    }

    @Test
    void useVerifyCommand(){
        Executable solver = new ExactEqValidator();
        console.runProcess(solver.getCommand("3*x**2 -2*x -y = -2*y + x - 3*y**2"));
        if (console.hasErrors()){
            System.out.println(console.getStdErr());
            fail();
        }else {
            System.out.println(console.getStdOut());
        }
    }

    @Test
    void useReduceCommand(){
        Executable solver = new ExactEqSolver();
        console.runProcess(solver.getCommand("3*x**2 -2*x -y = -2*y + x - 3*y**2"));
        if (console.hasErrors()){
            System.out.println(console.getStdErr());
            fail();
        }else {
            System.out.println(console.getStdOut());
        }
    }

    @Test
    void badSyntaxErrorCapture(){
        Executable solver = new ExactEqSolver();
        console.runProcess(solver.getCommand("3x = 3y"));
        if (console.hasErrors()){
            System.out.println(console.getStdErr());
        }else {
            System.out.println(console.getStdOut());
            fail();
        }
    }
}
