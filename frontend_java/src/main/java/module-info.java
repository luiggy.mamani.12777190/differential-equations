module com.example.frontend_java {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    opens com.example.frontend_java to javafx.fxml;
    exports com.example.frontend_java;
    exports com.example.frontend_java.controller;
    opens com.example.frontend_java.controller to javafx.fxml;
    exports com.example.frontend_java.frontendRequest;
    opens com.example.frontend_java.frontendRequest to javafx.fxml;
    exports com.example.frontend_java.backendResponses;
    opens com.example.frontend_java.backendResponses to javafx.fxml;
}