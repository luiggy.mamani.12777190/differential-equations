package com.example.frontend_java.components.keyboardContainer;

import com.example.frontend_java.components.keyboardContainer.keyButtonTypes.ButtonToClear;
import com.example.frontend_java.components.keyboardContainer.keyButtonTypes.ButtonToSend;
import com.example.frontend_java.components.keyboardContainer.keyButtonTypes.NormalButton;
import com.example.frontend_java.elements.KeyboardElements;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

public class Keyboard {

    private FlowPane container;
    private TextField inputEquation;
    private KeyboardElements elements;

    public Keyboard(TextField inputEquation) {
        this.inputEquation = inputEquation;
        elements = KeyboardElements.getInstance();
    }

    public FlowPane getContainer() {
        if (container == null) {
            container = elements.getContainer();
            buildButtons();
        }

        return container;
    }

    private void buildButtons() {

        container.getChildren().addAll(
                (new NormalButton(inputEquation, "^2", "/images/potency2.png")).getKeyButton(),
                (new NormalButton(inputEquation, "log()", "/images/logarithm.png")).getKeyButton(),
                (new NormalButton(inputEquation, "exp()", "/images/euler.png")).getKeyButton(),
                (new NormalButton(inputEquation, "tan()", "/images/tan.png")).getKeyButton(),
                (new NormalButton(inputEquation, "sin()", "/images/sin.png")).getKeyButton(),
                (new NormalButton(inputEquation, "cos()", "/images/cos.png")).getKeyButton(),
                (new ButtonToClear(inputEquation, "/images/delete.png")).getKeyButton(),
                (new NormalButton(inputEquation, "^()", "/images/potencyN.png")).getKeyButton(),
                (new NormalButton(inputEquation, "sqrt()", "/images/root2.png")).getKeyButton(),
                (new NormalButton(inputEquation, "x", "/images/symbolX.png")).getKeyButton(),
                (new NormalButton(inputEquation, "y", "/images/symbolY.png")).getKeyButton(),
                (new NormalButton(inputEquation, "()=()", "/images/equation.png")).getKeyButton(),
                (new ButtonToSend(inputEquation, "/images/equals.png")).getKeyButton()
        );
    }

}