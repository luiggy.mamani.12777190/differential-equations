package com.example.frontend_java.components.keyboardContainer.keyButtonTypes;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public interface Pressable {

    public Button getKeyButton();
    public void addEvent();
    public void startAction(TextField inputEquation);

}