package com.example.frontend_java.components;

import com.example.frontend_java.App;
import com.example.frontend_java.backendResponses.EquationAnswers;
import javafx.collections.ObservableList;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;

public class AnswersContainer {

    private WebView webView;
    private WebEngine webEngine;
    private EquationAnswers response;
    private final String HEADER;
    private final String FOOTER;
    private final String STYLE;
    private final String TITLE;
    private static AnswersContainer processContainer;

    public static AnswersContainer getInstance() {
        if (processContainer == null)
            processContainer = new AnswersContainer();

        return processContainer;
    }

    public AnswersContainer() {
        this.webView = new WebView();
        this.response = EquationAnswers.getInstance();
        this.HEADER = "<html><head><script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script></head><body><div>";
        this.FOOTER = "</div></body></html>";
        this.TITLE = "<h1 id='title'>GRANJITA</h1>";
        this.STYLE = (App.class.getResource("AnswersContainer.css")).toExternalForm();
    }

    public WebView getContainer() {
        webEngine = webView.getEngine();
        loadResponses();
        webEngine.setUserStyleSheetLocation(STYLE);

        return webView;
    }

    private String getBody(ObservableList<String> results) {
        StringBuilder body = new StringBuilder();
        body.append(HEADER);
        results.forEach(result -> {
            body.append("<h3>$$");
            body.append(result);
            body.append("$$</h3>");
        });
        body.append("<h6></h6>");
        body.append(FOOTER);

        return body.toString();
    }

    private String getBody() {
        return HEADER + TITLE + FOOTER;
    }

    public void loadResponses() {
        webEngine.loadContent(
                response.getResults().isEmpty() ?
                        getBody() :
                        getBody(response.getResults())
        );
        response.clear();
    }

}
