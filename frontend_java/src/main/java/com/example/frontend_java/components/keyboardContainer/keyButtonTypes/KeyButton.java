package com.example.frontend_java.components.keyboardContainer.keyButtonTypes;

import com.example.frontend_java.elements.KeyboardElements;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class KeyButton {

    protected TextField inputToWrite;
    protected Button keyButton;
    protected String textToWrite, urlIconImage;
    protected KeyboardElements elements;

    public KeyButton(TextField input, String textToWrite, String urlIconImage) {
        this.inputToWrite = input;
        this.textToWrite = textToWrite;
        this.urlIconImage = urlIconImage;
        this.elements = KeyboardElements.getInstance();
    }

    public KeyButton(TextField input, String urlIconImage) {
        this.inputToWrite = input;
        this.urlIconImage = urlIconImage;
        this.elements = KeyboardElements.getInstance();
    }
}