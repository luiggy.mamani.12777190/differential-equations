package com.example.frontend_java.components.keyboardContainer.keyButtonTypes;

import com.example.frontend_java.backendResponses.TypeOfMessage;
import com.example.frontend_java.components.AnswersContainer;
import com.example.frontend_java.components.EquationContainer;
import com.example.frontend_java.frontendRequest.InputEquation;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ButtonToSend extends KeyButton implements Pressable {

    private InputEquation inputEquation;

    public ButtonToSend(TextField input, String urlIconImage) {
        super(input, urlIconImage);
        this.inputEquation = InputEquation.getInstance();
    }

    @Override
    public Button getKeyButton() {
        if (keyButton == null) {
            keyButton = elements.getKeyButton(urlIconImage);
            keyButton.getStyleClass().add("ButtonToSend");
            addEvent();
        }

        return keyButton;
    }

    @Override
    public void addEvent() {
        keyButton.setOnAction(event -> {
            startAction(inputToWrite);
        });
    }

    @Override
    public void startAction(TextField inputEquation) {
        if (!inputEquation.getText().isEmpty()) {
            this.inputEquation.setEquation(inputEquation.getText());
            reloadData();
        } else {
            EquationContainer.getInstance()
                    .updateMessage("Missing equation", TypeOfMessage.DANGER);
        }
    }

    private void reloadData() {
        AnswersContainer.getInstance().loadResponses();
        EquationContainer.getInstance().updateMessage();
    }
}
