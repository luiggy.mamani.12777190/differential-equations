package com.example.frontend_java.components.keyboardContainer.keyButtonTypes;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ButtonToClear extends KeyButton implements Pressable {


    public ButtonToClear(TextField inputEquation, String urlIconImage) {
        super(inputEquation, urlIconImage);
    }

    @Override
    public Button getKeyButton() {
        if (keyButton == null) {
            keyButton = elements.getKeyButton(urlIconImage);
            keyButton.getStyleClass().add("ButtonToClear");
            addEvent();
        }

        return keyButton;
    }

    @Override
    public void addEvent() {
        keyButton.setOnAction(event -> {
            startAction(inputToWrite);
        });
    }

    @Override
    public void startAction(TextField inputEquation) {
        inputEquation.clear();
    }
}