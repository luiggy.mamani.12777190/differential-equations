package com.example.frontend_java.components;

import com.example.frontend_java.backendResponses.Alerts;
import com.example.frontend_java.backendResponses.TypeOfMessage;
import com.example.frontend_java.elements.InputElements;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class EquationContainer {

    private InputElements elements;
    private VBox parentContainer;
    private TextField inputEquation;
    private Label alertMessage;
    private Alerts alerts;
    private static EquationContainer inputComponent;

    public static EquationContainer getInstance() {
        if (inputComponent == null)
            inputComponent = new EquationContainer();

        return inputComponent;
    }

    public EquationContainer() {
        this.elements = new InputElements();
        this.alerts = Alerts.getInstance();
    }

    public VBox getContainer() {
        if (parentContainer == null) {
            buildElements();
            addElements();
        }

        return parentContainer;
    }

    private void buildElements() {
        this.parentContainer = elements.getInputContainer();
        this.inputEquation = elements.getInput("( Function M ) = ( Function N )");
        this.alertMessage = alerts.isEmpty() ? elements.getInputMessage()
                : elements.getInputMessage(alerts.getMessage(), alerts.getType());

        addListenerToInput();
    }

    private void addElements() {
        parentContainer.getChildren().add(inputEquation);
        loadMessage();
    }

    private void addListenerToInput() {
        inputEquation.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) && parentContainer.getChildren().contains(alertMessage)) {
                alertMessage.setText("");
                parentContainer.getChildren().remove(alertMessage);
            }
        });
    }

    public void updateMessage(String message, TypeOfMessage type) {
        alertMessage.setText(message.toLowerCase());
        alertMessage.getStyleClass().clear();
        alertMessage.getStyleClass().add(type.toString());
        loadMessage();
    }

    public void updateMessage() {
        if (!alerts.isEmpty())
            updateMessage(alerts.getMessage(), alerts.getType());
    }

    public void loadMessage() {
        if (!alertMessage.getText().isEmpty() && !parentContainer.getChildren().contains(alertMessage))
            parentContainer.getChildren().add(alertMessage);
    }

    public TextField getInputEquation() {
        return inputEquation;
    }
}
