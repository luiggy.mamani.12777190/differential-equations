package com.example.frontend_java.components.keyboardContainer.keyButtonTypes;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class NormalButton extends KeyButton implements Pressable {

    public NormalButton(TextField inputEquation, String textToWrite, String urlIconImage) {
        super(inputEquation, textToWrite, urlIconImage);
    }

    public Button getKeyButton() {
        if (keyButton == null) {
            keyButton = elements.getKeyButton(urlIconImage);
            addEvent();
        }

        return keyButton;
    }

    public void addEvent() {
        keyButton.setOnAction(event -> {
            startAction(inputToWrite);
        });
    }

    @Override
    public void startAction(TextField inputEquation) {
        inputEquation.setText(inputEquation.getText() + textToWrite);
        inputEquation.setFocusTraversable(true);
    }
}