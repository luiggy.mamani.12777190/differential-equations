package com.example.frontend_java;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Parent app = FXMLLoader.load(getClass().getResource("calculator.fxml"));
        Scene scene = new Scene(app, 950, 650);

        stage.setTitle("Granjita");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}