package com.example.frontend_java.backendResponses;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class EquationAnswers {

    private ObservableList<String> results;
    private static EquationAnswers response;

    public static EquationAnswers getInstance() {
        if (response == null)
            response = new EquationAnswers();

        return response;
    }

    public EquationAnswers() {
        this.results = FXCollections.observableArrayList();
    }

    public void setResults(ObservableList<String> results) {
        this.results = results;
    }

    public void setResults(String ...results) {
        this.results.addAll(results);
    }

    public void addResult(String result){
        results.add(result);
    }

    public ObservableList<String> getResults() {
        return results;
    }

    public void clear() {
        this.results.clear();
    }
}
