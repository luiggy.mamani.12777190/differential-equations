package com.example.frontend_java.backendResponses;

public enum TypeOfMessage {

    DANGER("dangerMessage"),
    INFO("infoMessage"),
    SUCCESS("successMessage"),
    WARNING("warningMessage");

    private final String messageStatus;

    TypeOfMessage(String stateMessage) {
        this.messageStatus = stateMessage;
    }

    @Override
    public String toString() {
        return messageStatus;
    }
}