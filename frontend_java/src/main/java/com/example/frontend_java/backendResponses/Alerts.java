package com.example.frontend_java.backendResponses;

public class Alerts {

    private String message;
    private TypeOfMessage type;
    private static Alerts alerts;

    public static Alerts getInstance() {
        if (alerts == null)
            alerts = new Alerts();

        return alerts;
    }

    public Alerts() {
        this.message = "";
        this.type = TypeOfMessage.INFO;
    }

    public void setMessage(String message, TypeOfMessage type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public TypeOfMessage getType() {
        return type;
    }

    public boolean isEmpty() {
        return this.message.isEmpty();
    }

    public void clear() {
        this.message = "";
    }
}