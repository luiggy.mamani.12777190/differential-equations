package com.example.frontend_java.controller.granjita;

import java.util.ArrayList;

import com.example.frontend_java.controller.granjita.commands.diff_equations.Solver;

/**
 * ProcessHandler
 */
public class GranjitaManager {
	public static String RUN_PREFIX = "poetry run granjita";
	private Solver solver;

	public GranjitaManager() {
		solver = new Solver();
	}

	public void requestSolution(String equation) {
		solver.solve(equation);
	}

	public void propagateSolution(String solution) {
		ArrayList<String> eqSolution = new ArrayList<>();
		for (String line : solution.split("\s")) {
			eqSolution.add(line);
		}
	}
}
