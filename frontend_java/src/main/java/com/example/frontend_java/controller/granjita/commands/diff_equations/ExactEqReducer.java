package com.example.frontend_java.controller.granjita.commands.diff_equations;

import java.util.List;

import com.example.frontend_java.controller.granjita.Executable;
import com.example.frontend_java.controller.granjita.GranjitaManager;
import com.example.frontend_java.controller.granjita.commands.ShellCommandHolder;

/**
 * Reduce
 */
public class ExactEqReducer implements Executable {

	private boolean usedNForm;

	@Override
	public List<String> getCommand(String equation) {
		ShellCommandHolder commands = new ShellCommandHolder();
		commands.strip(GranjitaManager.RUN_PREFIX);
		commands.addArgument("reduce");
		commands.addArgument(equation);
		useFormula(commands);
		return commands.getArguments();
	}

	private void useFormula(ShellCommandHolder commands) {
		if (usedNForm) {
			commands.addArgument("--formula");
			commands.addArgument("m");
		}
		usedNForm = !usedNForm;
	}

}
