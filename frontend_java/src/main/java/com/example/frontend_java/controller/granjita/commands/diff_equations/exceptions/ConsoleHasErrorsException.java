package com.example.frontend_java.controller.granjita.commands.diff_equations.exceptions;

/**
 * ConsoleHasErrorsException
 */
public class ConsoleHasErrorsException extends Exception{

	public ConsoleHasErrorsException() {
		super("console error at equation solving stage");
	}

	public ConsoleHasErrorsException(String message) {
		super(message);
	}
}
