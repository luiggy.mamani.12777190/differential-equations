package com.example.frontend_java.controller;


import com.example.frontend_java.components.EquationContainer;
import com.example.frontend_java.components.AnswersContainer;
import com.example.frontend_java.components.keyboardContainer.Keyboard;
import javafx.fxml.FXML;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

public class CalculatorController {
    @FXML
    private VBox mainContainer;
    private WebView processContainer;
    private VBox inputContainer;
    private FlowPane keysMenuContainer;

    public void initialize() {
        initializeComponents();
        addChildren();
    }

    private void initializeComponents() {
        processContainer = AnswersContainer.getInstance().getContainer();
        inputContainer = EquationContainer.getInstance().getContainer();
        keysMenuContainer = new Keyboard(EquationContainer.getInstance().getInputEquation())
                .getContainer();
    }

    private void addChildren() {
        mainContainer.getChildren().addAll(
                processContainer,
                inputContainer,
                keysMenuContainer
        );
    }
}