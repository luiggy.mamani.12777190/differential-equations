package com.example.frontend_java.controller.granjita.commands;

import java.util.ArrayList;

/**
 * Holder
 */
public class ShellCommandHolder {
	private ArrayList<String> command;

	public ShellCommandHolder() {
		this.command = new ArrayList<>();
	}

	public ShellCommandHolder addArgument(String arg) {
		command.add(arg);
		return this;
	}

	public ArrayList<String> getArguments() {
		return command;
	}

	public void strip(String commands) {
		String[] split = commands.split("\\s+");
		for (String command : split) {
			addArgument(command);
		}
	}
}
