package com.example.frontend_java.controller.granjita;

import java.util.List;

/**
 * Executable
 */
public interface Executable {
	List<String> getCommand(String equation);
}
