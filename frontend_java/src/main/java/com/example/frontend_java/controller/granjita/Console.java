package com.example.frontend_java.controller.granjita;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * ProcessHandler
 */
public class Console {
	private ProcessBuilder builder;
	private Process process;
	private StringWriter stdErrReader;
	private boolean hasError;

	public Console(){
		stdErrReader = new StringWriter();
		builder = new ProcessBuilder();
		File backend_python = new File("/home/fundacion/PycharmProjects/diff-equations/development/backend_python");
		builder.directory(backend_python);
	}
	
	public void runProcess(List<String> commands) {
		try {
			close();
			process = builder.command(commands).start(); 
			process.waitFor();
			read();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void read() {
		try {
			hasError = process.errorReader().ready();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Stream<String> getStdOut() {
		return process.inputReader().lines();
	}

	public boolean hasErrors() {
		return process.exitValue() != 0 || hasError;
	}

	public String getStdErr() {
		return streamToString(process.errorReader().lines().iterator()); 
	}

	public void close() {
		if (process != null  && process.isAlive()) {
			process.destroy();
		}
		process = null;
	}

	private String streamToString(Iterator<String> stream){
		StringBuilder builder = new StringBuilder();
		while (stream.hasNext()) {
			builder.append(stream.next() + "\n");
		}
		return builder.toString();
	}
}
