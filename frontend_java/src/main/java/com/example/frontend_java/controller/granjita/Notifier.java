package com.example.frontend_java.controller.granjita;

import java.util.ArrayList;

/**
 * Notifier
 */
public class Notifier {
	private ArrayList<Client> subscribers;
	private static Notifier self;

	private Notifier(){
		subscribers = new ArrayList<>();
	}

	public static Notifier getInstance(){
		if (self == null){
			self = new Notifier();
		}
		return self;
	}

	public void subscribe(Client subscriber){
		subscribers.add(subscriber);
	}

	public void notifyAll(ArrayList<String> notice){
		subscribers.forEach(sub -> sub.notify(notice));
	}
}
