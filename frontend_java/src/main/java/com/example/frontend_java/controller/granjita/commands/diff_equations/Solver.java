package com.example.frontend_java.controller.granjita.commands.diff_equations;

import java.util.Iterator;

import com.example.frontend_java.backendResponses.Alerts;
import com.example.frontend_java.backendResponses.EquationAnswers;
import com.example.frontend_java.backendResponses.TypeOfMessage;
import com.example.frontend_java.controller.granjita.Console;
import com.example.frontend_java.controller.granjita.Executable;
import com.example.frontend_java.controller.granjita.commands.diff_equations.exceptions.ConsoleHasErrorsException;

/**
 * EquationSolver
 */
public class Solver {

	private Console console;
	private Executable exactSolver;
	private Executable exactReducer;
	private ExactEqValidator exactChecker;

	public Solver() {
		exactSolver = new ExactEqSolver();
		exactReducer = new ExactEqReducer();
		exactChecker = new ExactEqValidator();
		console = new Console();
	}

	public void solve(String equation) {
		try {
			if (isExact(equation)) {
				solveExactEquation(equation);
			} else {
				String reducedEquation = getReducedEquation(equation);

				if (isExact(reducedEquation)) {
					solveExactEquation(reducedEquation);
				} else {
					reducedEquation = getReducedEquation(equation); // will use M form
					solveExactEquation(reducedEquation);
				}
			}
		} catch (ConsoleHasErrorsException e) {
			errorHandling();
		}
	}

	private boolean isExact(String equation) throws ConsoleHasErrorsException {
		console.runProcess(exactChecker.getCommand(equation));

		if (console.hasErrors()) {
			throw new ConsoleHasErrorsException("ERROR: while trying to check if the equation is exact");
		} else {
			Iterator<String> consoleOutput = console.getStdOut().iterator();
			String status = consoleOutput.next();
			saveProcedure("Check if equation is exact", consoleOutput);
			return exactChecker.extractOperationStatus(status);
		}
	}

	private void solveExactEquation(String equation) throws ConsoleHasErrorsException {
		console.runProcess(exactSolver.getCommand(equation));

		if (console.hasErrors()) {
			throw new ConsoleHasErrorsException("ERROR: while trying to solve the equation as exact");
		} else {
			saveProcedure("Solve equation", console.getStdOut().iterator());
		}
	}

	private String getReducedEquation(String equation) throws ConsoleHasErrorsException {
		console.runProcess(exactReducer.getCommand(equation));

		if (console.hasErrors()) {
			throw new ConsoleHasErrorsException("ERROR: while trying to reduce the equation");
		} else {
			Iterator<String> consoleOuput = console.getStdOut().iterator();
			String reducedEquation = consoleOuput.next();
			saveProcedure("Reduce equation", consoleOuput);
			return reducedEquation;
		}
	}

	private void errorHandling() {
		System.out.println(console.getStdErr());
//		Alerts.getInstance().setMessage(console.getStdErr(), TypeOfMessage.INFO);
	}

	private void saveProcedure(String title, Iterator<String> procedure) {
		// TODO: Titles are provided but are not being used.
		EquationAnswers answers = EquationAnswers.getInstance();
		while (procedure.hasNext()) {
			answers.addResult(procedure.next());
		}
	}
}
