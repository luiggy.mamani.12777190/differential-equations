package com.example.frontend_java.controller.granjita;

import java.util.List;

/**
 * Client
 */
public interface Client {

	void notify(List<String> list);
}
