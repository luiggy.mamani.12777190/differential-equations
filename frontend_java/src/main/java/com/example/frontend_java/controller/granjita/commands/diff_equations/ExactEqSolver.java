package com.example.frontend_java.controller.granjita.commands.diff_equations;

import java.util.List;

import com.example.frontend_java.controller.granjita.Executable;
import com.example.frontend_java.controller.granjita.GranjitaManager;
import com.example.frontend_java.controller.granjita.commands.ShellCommandHolder;

/**
 * Solve
 */
public class ExactEqSolver implements Executable {

	@Override
	public List<String> getCommand(String equation) {
		ShellCommandHolder commands = new ShellCommandHolder();
		commands.strip(GranjitaManager.RUN_PREFIX);
		commands.addArgument("solve");
		commands.addArgument(equation);
		return commands.getArguments();
	}
}
