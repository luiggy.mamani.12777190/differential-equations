package com.example.frontend_java.controller.granjita.commands.diff_equations;

import java.util.List;

import com.example.frontend_java.controller.granjita.Executable;
import com.example.frontend_java.controller.granjita.GranjitaManager;
import com.example.frontend_java.controller.granjita.commands.ShellCommandHolder;

/**
 * ExactEqValidator
 */
public class ExactEqValidator implements Executable{

	public boolean extractOperationStatus(String status){
		return Boolean.parseBoolean(status.trim().split("\\s*:\\s*")[1].toLowerCase());
	}

	@Override
	public List<String> getCommand(String equation) {
		ShellCommandHolder commands = new ShellCommandHolder();
		commands.strip(GranjitaManager.RUN_PREFIX);
		commands.addArgument("verify");
		commands.addArgument(equation);
		return commands.getArguments();
	}
	
}
