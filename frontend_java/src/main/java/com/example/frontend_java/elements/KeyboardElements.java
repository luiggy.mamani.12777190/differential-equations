package com.example.frontend_java.elements;

import com.example.frontend_java.App;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

import java.net.URISyntaxException;

public class KeyboardElements {

    private static KeyboardElements elements;

    public static KeyboardElements getInstance() {
        if (elements == null)
            elements = new KeyboardElements();

        return elements;
    }

    public FlowPane getContainer() {
        FlowPane menuContainer = new FlowPane();
        menuContainer.setAlignment(Pos.CENTER);
        menuContainer.setHgap(6);
        menuContainer.setVgap(6);
        menuContainer.setPadding(new Insets(5, 0, 5, 0));
        return menuContainer;
    }

    public Button getKeyButton(String urlIconImage) {
        Button button = new Button();
        button.setPrefSize(110, 25);
        button.setGraphic(getIconImage(urlIconImage, button.getPrefWidth(), button.getPrefHeight()));
        return button;
    }

    private ImageView getIconImage(String urlIconImage, double width, double height) {
        try {
            Image icon = new Image(App.class.getResource(urlIconImage).toURI().toString());
            ImageView iconImage = new ImageView(icon);
            iconImage.setPreserveRatio(true);
            iconImage.setFitWidth(width);
            iconImage.setFitHeight(height);
            return iconImage;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

}