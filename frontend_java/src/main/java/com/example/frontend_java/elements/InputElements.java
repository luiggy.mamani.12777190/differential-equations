package com.example.frontend_java.elements;

import com.example.frontend_java.backendResponses.TypeOfMessage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class InputElements {


    public VBox getInputContainer() {
        VBox container = new VBox();
         container.setSpacing(5);
        return container;
    }

    public TextField getInput(String promptText) {
        TextField input = new TextField();
        input.setPrefHeight(120);
        input.setAlignment(Pos.CENTER);
        input.setPadding(new Insets(10, 10, 25, 10));
        VBox.setMargin(input, new Insets(0, 120, 0, 120));
        input.setPromptText(promptText);
        input.getStyleClass().add("equationInput");

        return input;
    }

    public Label getInputMessage(String message, TypeOfMessage type) {
        Label inputMessage = getInputMessage();
        inputMessage.setText(message.toLowerCase());
        inputMessage.getStyleClass().add(type.toString());

        return inputMessage;
    }

    public Label getInputMessage() {
        Label inputMessage = new Label();
        inputMessage.setWrapText(true);
        inputMessage.setPrefWidth(Double.MAX_VALUE);
        inputMessage.setAlignment(Pos.CENTER);

        return inputMessage;
    }

}