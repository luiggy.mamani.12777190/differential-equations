package com.example.frontend_java.frontendRequest;

import com.example.frontend_java.controller.granjita.GranjitaManager;

public class InputEquation {

    private String equationToSolve;
    private static GranjitaManager manager;
    private static InputEquation inputEquation;

    public static InputEquation getInstance() {
        if (inputEquation == null){
            inputEquation = new InputEquation();
            manager = new GranjitaManager();
        }

        return inputEquation;
    }

    public InputEquation() {
        this.equationToSolve = "";
    }

    public void setEquation(String equationToSolve) {
        this.equationToSolve = equationToSolve;
        manager.requestSolution(equationToSolve);
    }
}
