import sys
sys.path.append("./backend_python/src")

from model.differential_equations.term import Term
from model.differential_equations.function import Function
from model.differential_equations.equation import Equation
from model.logic.solvers.exact_equation.solver import Solver
from model.differential_equations.variables import *
import sympy as sp
import unittest



class TestExactDiffEquation(unittest.TestCase):

    def test_base_case(self):
        # (3x^2 - 2x-y)dx   =  (-2y+x-3y^2)dy
        term1 = Term("3*x**2")
        term2 = Term("-2*x")
        term3 = Term("-y")

        term4 = Term("-2*y")
        term5 = Term("x")
        term6 = Term("-3*y**2")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)
        function1.add_term(term3)

        function2 = Function()
        function2.add_term(term4)
        function2.add_term(term5)
        function2.add_term(term6)

        equation = Equation(function1, function2)

        solver = Solver()

        procedure = solver.solve(equation)

        expected = sp.Eq(M, 3*x**2 - 2*x - y)
        actual = procedure[0]
        self.assertEqual(expected, actual)

        expected = sp.Eq(N, -x + 3*y**2 + 2*y)
        actual = procedure[1]
        self.assertEqual(expected, actual)

        expected = sp.Eq(M1, sp.Integral(3*x**2 - 2*x - y, x))
        actual = procedure[2]
        self.assertEqual(expected, actual)

        expected = sp.Eq(M1, x**3 - x**2 - x*y)
        actual = procedure[3]
        self.assertEqual(expected, actual)

        expected = sp.Eq(sp.Derivative(M1, y), x**3 - x**2 - x*y)
        actual = procedure[4]
        self.assertEqual(expected, actual)

        expected = sp.Eq(sp.Derivative(M1, y), -x)
        actual = procedure[5]
        self.assertEqual(expected, actual)

        expected = sp.Eq(N1, sp.Integral(
            N - sp.Derivative(M1, y), y))
        actual = procedure[6]
        self.assertEqual(expected, actual)

        expected = sp.Eq(N1, sp.Integral(3*y**2 + 2*y, y))
        actual = procedure[7]
        self.assertEqual(expected, actual)

        expected = sp.Eq(N1, y**3 + y**2)
        actual = procedure[8]
        self.assertEqual(expected, actual)

        expected = sp.Eq(M1 + N1, C1)
        actual = procedure[9]
        self.assertEqual(expected, actual)

        expected = sp.Eq(x**3 - x**2 - x*y + y**3 + y**2, C1)
        actual = procedure[10]
        self.assertEqual(expected, actual)

    def test_trgonometric_function(self):
        # (seny - ysenx)dx   =  (-cosx-xcosy+y)dy

        term1 = Term("sin(y)")
        term2 = Term("-y*sin(x)")

        term3 = Term("-cos(x)")
        term4 = Term("-x*cos(y)")
        term5 = Term("y")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)
        function2.add_term(term4)
        function2.add_term(term5)

        equation = Equation(function1, function2)

        solver = Solver()

        trigonometric_procedure = solver.solve(equation)

        general_solution_expected = sp.Eq(
            x*sp.sin(y) - y**2/2 + y*sp.cos(x), C1)
        general_solution_actual = trigonometric_procedure[10]
        self.assertEqual(general_solution_expected, general_solution_actual)


    def test_trgonometric_function(self):
        # (seny - ysenx)dx   =  (-cosx-xcosy+y)dy

        term1 = Term("sin(y)")
        term2 = Term("-y*sin(x)")

        term3 = Term("-cos(x)")
        term4 = Term("-x*cos(y)")
        term5 = Term("y")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)
        function2.add_term(term4)
        function2.add_term(term5)

        equation = Equation(function1, function2)

        solver = Solver()

        trigonometric_procedure = solver.solve(equation)

        general_solution_expected = sp.Eq(
            x*sp.sin(y) - y**2/2 + y*sp.cos(x), C1)
        general_solution_actual = trigonometric_procedure[10]
        self.assertEqual(general_solution_expected, general_solution_actual)

    def test_euler_function(self):
        # (e^y +6x)dx   =  (xe^y)dy

        term1 = Term("exp(y)")
        term2 = Term("6*x")

        term3 = Term("x*exp(y)")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)

        equation = Equation(function1, function2)

        solver = Solver()

        procedure = solver.solve(equation)

        general_solution_expected = sp.Eq(3*x**2 - x*sp.exp(y), C1)
        general_solution_actual = procedure[10]
        self.assertEqual(general_solution_expected, general_solution_actual)


if __name__ == '__main__':
    unittest.main()