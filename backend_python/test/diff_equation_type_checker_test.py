import sys
sys.path.append("./backend_python/src")

from model.differential_equations.term import Term
from model.differential_equations.function import Function
from model.differential_equations.equation import Equation
from model.differential_equations.variables import *
import sympy as sp
import unittest
from model.logic.solvers.exact_equation.verifier import is_exact


class TestExactDiffEquation(unittest.TestCase):

    def test_base_case(self):
        # [3x^2-2x-y]dx   =  (-2y+x+3y^2)dy
        
        term1 = Term("3*x**2")
        term2 = Term("-2*x")
        term3 = Term("-y")

        term4 = Term("-2*y")
        term5 = Term("x")
        term6 = Term("-3*y**2")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)
        function1.add_term(term3)

        function2 = Function()
        function2.add_term(term4)
        function2.add_term(term5)
        function2.add_term(term6)

        equation = Equation(function1, function2)

        expected = True
        actual, procedure = is_exact(equation)
        self.assertEqual(expected, actual)
        
       

        expectedProcedure = sp.Eq(M*dx + N*dy, 0)
        actualProcedure = procedure[0]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(M, 3*x**2 - 2*x - y)
        actualProcedure = procedure[1]
        self.assertEqual(expectedProcedure, actualProcedure)
        
        expectedProcedure = sp.Eq(N, -x + 3*y**2 + 2*y)
        actualProcedure = procedure[2]
        self.assertEqual(expectedProcedure, actualProcedure)


        expectedProcedure = sp.Eq(sp.Derivative(M, y), sp.Derivative(N, x))
        actualProcedure = procedure[3]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(-1, -1,evaluate=False)
        actualProcedure = procedure[4]
        self.assertEqual(expectedProcedure, actualProcedure)
    

    def test_trgonometric_function(self):
        # (seny - ysenx)dx  = (-cosx-xcosy+y)dy

        x, y = sp.symbols("x y")
        term1 = Term("sin(y)")
        term2 = Term("-y*sin(x)")

        term3 = Term("-cos(x)")
        term4 = Term("-x*cos(y)")
        term5 = Term("y")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)
        function2.add_term(term4)
        function2.add_term(term5)

        equation = Equation(function1, function2)

        expected = True
        actual, procedure = is_exact(equation)

        self.assertEqual(expected, actual)

        expectedProcedure =sp.Eq(M*dx + N*dy, 0)
        actualProcedure = procedure[0]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure =sp.Eq(M, -y*sp.sin(x) + sp.sin(y))
        actualProcedure = procedure[1]
        self.assertEqual(expectedProcedure, actualProcedure)
        
        expectedProcedure = sp.Eq(N, x*sp.cos(y) - y + sp.cos(x))
        actualProcedure = procedure[2]
        self.assertEqual(expectedProcedure, actualProcedure)


        expectedProcedure = sp.Eq(sp.Derivative(M, y), sp.Derivative(N, x))
        actualProcedure = procedure[3]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(-sp.sin(x) + sp.cos(y), -sp.sin(x) + sp.cos(y),evaluate=False)
        actualProcedure = procedure[4]
        self.assertEqual(expectedProcedure, actualProcedure)
    

    def test_euler_function(self):
        # (e^y +6x)dx   =  (xe^y)dy

        x, y = sp.symbols("x, y")
        term1 = Term("exp(y)")
        term2 = Term("6*x")

        term3 = Term("-x*exp(y)")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)

        equation = Equation(function1, function2)

        expected = True
        actual, procedure = is_exact(equation)


        self.assertEqual(expected, actual)
        expectedProcedure =sp.Eq(M*dx + N*dy, 0)
        actualProcedure = procedure[0]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure =sp.Eq(M, 6*x + sp.exp(y))
        actualProcedure = procedure[1]
        self.assertEqual(expectedProcedure, actualProcedure)
        
        expectedProcedure = sp.Eq(N, x*sp.exp(y))
        actualProcedure = procedure[2]
        self.assertEqual(expectedProcedure, actualProcedure)


        expectedProcedure = sp.Eq(sp.Derivative(M, y), sp.Derivative(N, x))
        actualProcedure = procedure[3]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(sp.exp(y), sp.exp(y),evaluate =False)
        actualProcedure = procedure[4]
        self.assertEqual(expectedProcedure, actualProcedure)
 

    def test_lineal_function(self):
        # (3x+4y+2)dx   =  (7x-2y+4)dy

        x, y = sp.symbols("x y")
        term1 = Term("3*x")
        term2 = Term("4*y")
        term3 = Term("2")

        term4 = Term("7*x")
        term5 = Term("-2*y")
        term6 = Term("4")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)
        function1.add_term(term3)

        function2 = Function()
        function2.add_term(term4)
        function2.add_term(term5)
        function2.add_term(term6)

        equation = Equation(function1, function2)

        expected = False
        actual, procedure = is_exact(equation)
            
        self.assertEqual(expected, actual)
        
        expectedProcedure =sp.Eq(M*dx + N*dy, 0)
        actualProcedure = procedure[0]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(M,  3*x + 4*y + 2)
        actualProcedure = procedure[1]
        self.assertEqual(expectedProcedure, actualProcedure)
        
        expectedProcedure =sp.Eq(N,-7*x + 2*y - 4)
        actualProcedure = procedure[2]
        self.assertEqual(expectedProcedure, actualProcedure)


        expectedProcedure = sp.Eq(sp.Derivative(M, y), sp.Derivative(N, x))
        actualProcedure = procedure[3]
        self.assertEqual(expectedProcedure, actualProcedure)

        expectedProcedure = sp.Eq(4, -7,evaluate=False)
        actualProcedure = procedure[4]
        self.assertEqual(expectedProcedure, actualProcedure)

if __name__ == '__main__':
    unittest.main()
