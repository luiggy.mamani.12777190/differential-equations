import sys
sys.path.append("./backend_python/src")

from model.differential_equations.term import Term
from model.logic.solvers.exact_equation.integration_factor_type import IFactorType
from model.logic.solvers.exact_equation.reducer import Reducer
from model.differential_equations.function import Function
from model.differential_equations.variables import *
from model.differential_equations.equation import Equation
import sympy as sp
import unittest



class TestExactDiffEquation(unittest.TestCase):

    def test_base_case(self):
        # (x+2y**2)dx   =  (x*y)dy

        term1 = Term("x")
        term2 = Term("2*y**2")
        term3 = Term("-x*y")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)

        equation = Equation(function1, function2)

        reducer = Reducer()

        newExactEquation, procedure = reducer.reduce(equation,IFactorType.CALCULATE_BY_N)

        expected = x**4+2*x**3*y**2
        actual = newExactEquation.function_M.terms_sympy

        self.assertEqual(expected, actual)

        expected = -x**4*y
        actual = newExactEquation.function_N.terms_sympy

        self.assertEqual(expected, actual)


    def test_base_case2(self):
        # (xy+y**2)dx   =  -(x**2+3xy)dy
    
        term1 = Term("x*y")
        term2 = Term("y**2")
        term3 = Term("-x**2")
        term4 = Term("-3*x*y")

        function1 = Function()
        function1.add_term(term1)
        function1.add_term(term2)

        function2 = Function()
        function2.add_term(term3)
        function2.add_term(term4)
        
        equation = Equation(function1, function2)

        reducer = Reducer()

        newExactEquation, procedure = reducer.reduce(equation,IFactorType.CALCULATE_BY_M)
             
        expected = x*y**2+y**3
        actual = newExactEquation.function_M.terms_sympy

        self.assertEqual(expected, actual)

        expected = -x**2*y-3*x*y**2
        actual = newExactEquation.function_N.terms_sympy

         
        self.assertEqual(expected, actual)


    def test_trigonometric_functions(self):
        # (y*cosx)dx   =  (-y+sin(x))dy

        term1 = Term("y*cos(x)")
        term2 = Term("-y")
        term3 = Term("sin(x)")

        function1 = Function()
        function1.add_term(term1)
        

        function2 = Function()
        function2.add_term(term2)
        function2.add_term(term3)

        equation = Equation(function1, function2)

        reducer = Reducer()

        newExactEquation, procedure = reducer.reduce(equation,IFactorType.CALCULATE_BY_M)


        expected = sp.cos(x)/y
        actual = newExactEquation.function_M.terms_sympy

        self.assertEqual(expected, actual)

        expected = (-y + sp.sin(x))/y**2
        actual = newExactEquation.function_N.terms_sympy

        self.assertEqual(expected, actual)

        processExpected = sp.Eq(-1/M*((sp.Derivative(M, y) - sp.Derivative(N, x))*dx), dv/v)
        processActual = procedure[0]
        self.assertEqual(processExpected,processActual)

        processExpected = sp.Eq(-2*dx/y, dv/v)
        processActual = procedure[1]
        self.assertEqual(processExpected,processActual)

        processExpected = sp.Eq(sp.Integral(-2/y, x), sp.Integral(1/v, v))
        processActual = procedure[2]
        self.assertEqual(processExpected,processActual)

        processExpected = sp.Eq(-2*sp.log(y), sp.log(v))
        processActual = procedure[3]
        self.assertEqual(processExpected,processActual)

        processExpected =sp.Eq(v, sp.exp(-2*sp.log(y),evaluate=False))
        processActual = procedure[4]
        self.assertEqual(processExpected,processActual)

        processExpected = sp.Eq(v, y**(-2))
        processActual = procedure[5]
        self.assertEqual(processExpected,processActual)

        processExpected = sp.Eq(sp.cos(x)/y, (-y + sp.sin(x))/y**2)
        processActual = procedure[6]
        self.assertEqual(processExpected,processActual)
        

if __name__ == '__main__':
    unittest.main()
