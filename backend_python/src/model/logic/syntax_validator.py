from sympy import *
from model.error_management.log_type import Log_Type
from model.error_management.alert import Alert
from model.handle.equation_builder import EquationBuilder
from model.differential_equations.variables import x, y, a

class Syntax_Validator:
    alert: Alert = None
    equation_builder: EquationBuilder = None

    def __init__(self):

        self.alert = Alert.get_instance()
        self.equation_builder = EquationBuilder()

    def is_valid(self, equation: str) -> bool:
        is_input_valid = False
        if equation != "" and self._is_valid_equation(equation):
            equation_built = self.equation_builder.build_str_equation(equation)
            is_input_valid = self.is_valid_function(equation_built.function_M.__str__(
            )) and self.is_valid_function(equation_built.function_N.__str__())
        return is_input_valid

    def is_valid_function(self, function: str):
        return self._is_logical_operation(function) and self._is_valid_expression(function)

    def _is_valid_expression(self, expression: str) -> bool:
        is_valid = False
        try:
            sympy_expression = sympify(expression)
            is_valid = self._contains_valid_variables(sympy_expression)
        except (SyntaxError, TypeError, NameError, SympifyError) as e:
            if isinstance(e, SympifyError):
                self._save_syntax_error_sympy(e)
            else:
                self.alert.save_message(str(f"Error: {e}"), Log_Type.ERROR)

        return is_valid

    def _is_logical_operation(self, expression: str) -> bool:
        if expression.find("/0") != -1:
            self.alert.save_message("Error: zero division", Log_Type.ERROR)
            return False

        return True

    def _contains_valid_variables(self, expression) -> bool:
        contain = True
        undefined_vars = expression.free_symbols - {x, y}
        if undefined_vars:
            contain = False
            self._save_invalid_variables(undefined_vars)

        return contain

    def _save_invalid_variables(self, undefined_vars) -> None:
        self.alert.save_message(str(
            f"Error: undefined {', '.join(str(v) for v in undefined_vars)}"), Log_Type.ERROR)

    def _save_syntax_error_sympy(self, sympy_error: SympifyError) -> None:
        error = str(sympy_error.args[1])
        error = error.replace("'EOF in multi-line statement', (2, 0))", "")
        error = error.replace("(<string>, line 1)", "")
        self.alert.save_message(
            str(f"Error: {sympy_error.expr} \n> {error}"), Log_Type.ERROR)

    def _is_valid_equation(self, equation: str) -> bool:
        is_valid = True
        if equation[0] == "=" or equation[len(equation)-1] == "=" or "=" not in equation:
            is_valid = False
            self.alert.save_message(
                    "The input inserted is not a valid equation", Log_Type.ERROR)
        return is_valid
