from model.logic.solvers.exact_equation.integration_factor_type import IFactorType
from model.logic.solvers.exact_equation.reduction_procedure_builder import ReductionProcedureBuilder
from sympy import Mul, cancel, diff, integrate, solve, factor
from model.differential_equations.variables import  x,y,v


class IntegrationFactorFinder:
    def __init__(self):
        self._procedure_builder = ReductionProcedureBuilder()

    def calculate(self,function_m:Mul,function_n:Mul,calculateType: IFactorType) -> tuple:
        dm_y = diff(function_m, y)#dM/dy
        dn_x = diff(function_n, x)#dN/dx
        procedure = []
        integrate_factor = None
        if calculateType == IFactorType.CALCULATE_BY_N:
            #1/N * (dM/dy-dN/dx)dx = dv/v
            integrate_factor, procedure = self._calculate_by_formula(
                dm_y, dn_x, function_n, calculateType)
        else:
            #-1/M * (dM/dy-dN/dx)dy = dv/v
            integrate_factor, procedure = self._calculate_by_formula(
                dm_y, dn_x, function_m, calculateType)
        return (integrate_factor, procedure)

    def _calculate_by_formula(self, dm_y: Mul, dn_x: Mul, function: Mul, calculateType: IFactorType) -> tuple:
        dependent_var, multiplying = self._define_formula(calculateType, function)

        product = multiplying*(dm_y-dn_x)
        product = self.symplify_term(product)
        # integrate (product) = integrate(dv/v)
        integrate_if = integrate(product, dependent_var)
        integrate_v = integrate((v)**-1,v)
        # v = e ^ ln(integrate factor)
        integration_factor = solve(integrate_if-integrate_v, v)[0]
       
        procedure = self._procedure_builder.build_integrate_factor(
            dm_y, dn_x, function, integrate_if, integrate_v, integration_factor, calculateType)

        return (integration_factor, procedure)

    def _define_formula(self, calculateType: IFactorType, function: Mul) -> tuple:
        multiplying = None
        if calculateType == IFactorType.CALCULATE_BY_N:
            #1/N * (dM/dy-dN/dx)dx = dv/v
            multiplying = (function)**-1
            dependent_var = x
        else:
            #-1/M * (dM/dy-dN/dx)dy = dv/v
            multiplying = (-1*function)**-1
            dependent_var = y
        return (dependent_var, multiplying)

    def symplify_term(self, terms: Mul) -> Mul:
        terms = factor(terms)
        terms = cancel(terms)
        return terms
