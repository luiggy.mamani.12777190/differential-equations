from model.differential_equations.equation import Equation
from model.logic.solvers.exact_equation.builder import Builder
from model.differential_equations.variables import *
from sympy import Symbol,integrate,diff


class Solver:

    def __init__(self) -> None:
        self._builder_procedure = Builder()

    def solve(self, equation: Equation) -> list:

        # Mdx + Ndy = 0
        function_m = equation.function_M.terms_sympy
        function_n = equation.function_N.terms_sympy * -1

        # Integrate M as a function of x
        m_prime = integrate(function_m, x)

        # deriving M1 as a function of y

        m_prime_dy = diff(m_prime, y)
        m_prime_dy = m_prime_dy*-1
        # Substracting N - dM/dy
        substraction = function_n+m_prime_dy

        # Integrate as a function of 'y' the substraction of (N - dM/dy)
        n_prime = integrate(substraction, y)
        procedure = self._builder_procedure.build_process(
            function_n, function_m, n_prime, m_prime, m_prime_dy)


        return procedure
