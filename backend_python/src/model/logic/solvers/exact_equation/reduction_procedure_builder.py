from model.logic.solvers.exact_equation.integration_factor_type import IFactorType
from sympy import Mul, Eq, Derivative, Integral, exp, Equality, Symbol
from model.differential_equations.variables import *


class ReductionProcedureBuilder():
    def __init__(self):
        self._procedure: list = []
        self.du: Symbol = None
        self.u: Symbol = None

    def build_integrate_factor(self, m_prime: Mul, n_prime: Mul, multiplying: Mul, integrate_if: Mul, integrate_v: Mul, integrate_if_result: Mul, formula: IFactorType) -> list:
        if formula == IFactorType.CALCULATE_BY_N:
            # 1/N * (dM/dy-dN/dx)dx = dv/v
            self._build_formula_by_n()
        else:
            # -1/M * (dM/dy-dN/dx)dy = dv/v
            self._build_formula_by_m()
            multiplying *= -1
        self._build_integration(m_prime, n_prime, multiplying,
                                integrate_if, integrate_v, integrate_if_result)
        return self._procedure

    # integrate (product) = integrate(dv/v)
    def _build_integration(self, m_prime: Mul, n_prime: Mul, dividor: Mul, integrate_if: Mul, integrate_v: Mul, integrate_if_result: Mul) -> None:
        self._procedure.append(
            Eq(1/dividor*((m_prime - n_prime)*self.du), dv/v),)
        self._procedure.append(
            Eq(Integral(1/dividor*(m_prime+(n_prime*-1)), self.u), Integral(1/v, v)))
        self._procedure.append(Eq(integrate_if, integrate_v))
        # v = e ^ ln(integration factor)
        self._procedure.append(
            Equality(v, exp(integrate_if, evaluate=False)))
        # v = integration factor
        self._procedure.append(Eq(v, integrate_if_result))

    # -1/M * (dM/dy-dN/dx)dy = dv/v
    def _build_formula_by_m(self) -> Eq:
        self._procedure.append(
            Eq(-1/M*((Derivative(M, y) - Derivative(N, x))*dy), dv/v))
        self.du = dy
        self.u = y

    # 1/N * (dM/dy-dN/dx)dx = dv/v
    def _build_formula_by_n(self) -> Eq:
        self._procedure.append(
            Eq(1/N*((Derivative(M, y) - Derivative(N, x))*dx), dv/v))
        self.du = dx
        self.u = x