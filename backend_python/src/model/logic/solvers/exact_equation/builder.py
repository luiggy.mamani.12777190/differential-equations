from sympy import Mul,Eq,Derivative,Integral,integrate,diff
from model.differential_equations.variables import *

class Builder:
    def __init__(self):
        self._procedure :list = []

    def build_process(self, n: Mul, m: Mul, n_prime: Mul, m_prime: Mul, m_primedy: Mul) -> list:
        self._define_functions(m, n)
        self._get_m_prime(m)
        self._derivate_m_prime(m_prime)
        self._get_n_prime(n, m_primedy, n_prime)
        self._join_functions(m_prime, n_prime)
        return self._procedure
    
    # Mdx + Ndy = 0
    def _define_functions(self, function_m: Mul, function_n: Mul):
        self._procedure.append(Eq(M, function_m))
        self._procedure.append(Eq(N, function_n))
    
    
    # Integrate M as a function of x
    def _get_n_prime(self, n: Mul, m_1dy: Mul, n_1: Mul) -> None:
        self._procedure.append(Eq(N1, Integral(
            (N-Derivative(M1, y)), y)))
        self._procedure.append(Eq(N1, Integral((n + m_1dy), y)))
        self._procedure.append(Eq(N1, n_1))


    # Integrate as a function of 'y' the substraction of (N - dM/dy)
    def _get_m_prime(self, m: Mul) -> None:
        self._procedure.append(Eq(M1, Integral(m, x)))
        self._procedure.append(Eq(M1, integrate(m, x)))


    
    # deriving M1 as a function of y
    def _derivate_m_prime(self, m_1: Mul) -> None:
        self._procedure.append(Eq(Derivative(M1, y), m_1))
        self._procedure.append(
            Eq(Derivative(M1, y), diff(m_1, y)))
    
   # M_prime + N_prime = C1
    def _join_functions(self, m_prime: Mul, n_prime: Mul):
        self._procedure.append(Eq(N1+M1, C1))
        self._procedure.append(Eq(m_prime+n_prime, C1))

    def build_verification(self, n: Mul, m: Mul, m_prime: Mul, n_prime: Mul) -> list:
        procedure = []
        procedure.append(Eq(M*dx+N*dy, 0))
        procedure.append(Eq(M, m))
        procedure.append(Eq(N, n))
        procedure.append(Eq(Derivative(M, y),Derivative(N, x)))
        procedure.append(Eq(m_prime, n_prime, evaluate=False))
        return procedure
