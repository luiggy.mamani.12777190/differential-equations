from model.differential_equations.equation import Equation
from model.handle.equation_builder import EquationBuilder
from model.logic.solvers.exact_equation.integration_factor_finder import IntegrationFactorFinder
from model.logic.solvers.exact_equation.integration_factor_type import IFactorType

from sympy import Equality


class Reducer:

    def __init__(self) -> None:
        self._equation_builder = EquationBuilder()
        self._integrate_factor = IntegrationFactorFinder()

    def reduce(self, equation: Equation,integrate_type:IFactorType) -> tuple:
        m = equation.function_M.terms_sympy
        n = equation.function_N.terms_sympy

        integrate_factor, procedure = self._integrate_factor.calculate(m,n*-1,integrate_type)
        
        m = m*integrate_factor
        n = n*integrate_factor

        m = self._integrate_factor.symplify_term(m)
        n = self._integrate_factor.symplify_term(n)
       
        exact_equation = self._equation_builder.build_mul_equation(m, n)
        procedure.append(Equality(exact_equation.function_M.terms_sympy, exact_equation.function_N.terms_sympy, evaluate=False))
        return (exact_equation, procedure)
