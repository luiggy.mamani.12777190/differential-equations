from model.differential_equations.equation import Equation
from model.logic.solvers.exact_equation.builder import Builder
from model.differential_equations.variables import x , y
from sympy import diff


def is_exact(equation: Equation) -> tuple:

    m = equation.function_M.terms_sympy
    n = equation.function_N.terms_sympy * -1

    dm_y = diff(m, y)

    dn_x = diff(n, x)

    builder = Builder()
    procedure = builder.build_verification(n, m, dm_y, dn_x)
    return (dm_y == dn_x, procedure)
