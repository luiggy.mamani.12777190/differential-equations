from enum import Enum
from colorama import Fore


class Log_Type(Enum):
    INFO = Fore.WHITE
    SUCCESS = Fore.GREEN
    WARNING = Fore.YELLOW
    ERROR = Fore.RED
