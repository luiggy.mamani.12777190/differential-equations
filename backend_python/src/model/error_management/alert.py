from model.error_management.log_type import Log_Type


class Alert:
    __instance = None
    message: str = ''
    type: Log_Type = Log_Type.INFO

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Alert.__instance is None:
            Alert.__instance = Alert()
        return Alert.__instance

    def clear(self):
        self.message = ''
        self.type = Log_Type.INFO

    def is_empty(self) -> bool:
        return self.message == ''

    def save_message(self, message: str, type: Log_Type) -> None:
        self.message = message
        self.type = type
