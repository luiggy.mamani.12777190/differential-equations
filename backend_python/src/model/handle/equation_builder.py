from model.differential_equations.equation import Equation
from model.differential_equations.function import Function
from model.differential_equations.term import Term
import sympy as sp


class EquationBuilder:
    _operators: list

    def __init__(self) -> None:
        self._operators = ["+", "-"]

    def build_equation(self, function_M: str, function_N: str) -> Equation:
        return Equation(
            self._build_function(function_M), self._build_function(function_N)
        )

    def build_str_equation(self, equation: str) -> Equation:
        functions: list[str] = equation.split("=")
        return self.build_mul_equation(sp.sympify(functions[0]), sp.sympify(functions[1]))

    def build_mul_equation(self, function_m: sp.Add, function_n: sp.Add) -> Equation:
        equation = Equation(self._build_mul_function(function_m), self._build_mul_function(function_n))
        return equation

    def _build_mul_function(self,terms: sp.Add) -> Function:
        function = Function()
        terms = sp.Add.make_args(terms)
        for term in terms:
            function.add_term(Term(str(term)))
        return function

    def _build_function(self, function_string: str) -> Function:
        term_string = ""
        function = Function()

        for index in range(len(function_string)):
            char = function_string[index]
            if self._is_operator(char):
                function.add_term(Term(term_string))
                term_string = char
            elif index == len(function_string) - 1:
                term_string += char
                function.add_term(Term(term_string))
            else:
                term_string += char

        return function

    def _is_operator(self, character: str) -> bool:
        return character in self._operators
