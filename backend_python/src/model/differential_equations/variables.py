from sympy import Symbol

x = Symbol("x")
y = Symbol("y")
v = Symbol("v")
M1 = Symbol("M1")
N1 = Symbol("N1")
M = Symbol("M")
N = Symbol("N")
dx = Symbol("d"+str(x), commutative=False)
dy = Symbol("d"+str(y), commutative=False)
dv = Symbol("d"+str(v))
C1 = Symbol("C1")
a = Symbol("a")
