import sympy
from sympy import sympify


class Term:
    _value: sympy = None
    _value_string: str = None

    def __init__(self, value: str):
        self._value_string = value

    @property
    def value(self) -> sympy:
        if self._value is None:
            self._value = sympify(self._value_string)

        return self._value

    @value.setter
    def value(self, value: str) -> None:
        self._value_string = value
        self._value = None

    def contains_variable(self, variable) -> bool:
        contains = False

        if isinstance(variable, str) and len(variable):
            contains = self._value_string.find(variable) != -1

        return contains

    def is_positive(self) -> bool:
        return self._value_string[0] != "-"

    def __str__(self) -> str:
        return self._value_string
