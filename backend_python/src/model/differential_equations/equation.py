from model.differential_equations.function import Function
from sympy import Equality, Eq


class Equation:
    _function_M: Function = None
    _function_N: Function = None
    _equation: Equality = None

    def __init__(self, function_M: Function = None, function_N: Function = None):
        self._function_M = function_M
        self._function_N = function_N

    @property
    def function_M(self) -> Function:
        return self._function_M

    @property
    def function_N(self) -> Function:
        return self._function_N

    @property
    def equation(self) -> Equality:
        return Eq(self._function_M.terms_sympy, self._function_N.terms_sympy)

    @function_M.setter
    def function_M(self, funcion_M: Function) -> None:
        self._function_M = funcion_M

    @function_N.setter
    def function_N(self, function_N: Function) -> None:
        self._function_N = function_N

    def __str__(self):
        return f"{self._function_M.__str__()} = {self._function_N.__str__()}"
