import sympy
from sympy import sympify


class Function:
    _terms: list = None
    _terms_string: str = None
    _terms_sympy: sympy = None

    def __init__(self) -> None:
        self._terms = []

    def add_term(self, term: sympy) -> None:
        self._terms.append(term)

    def add_terms(self, terms: ...) -> None:
        for term in terms:
            self.add_term(term)

    @property
    def terms(self) -> list:
        return self._terms

    @property
    def terms_sympy(self) -> sympy:
        return sympify(self.__str__())

    def clear(self) -> None:
        self._terms = []
        self._terms_string = None


    def _build_terms(self) -> str:
        terms: str = ""
        for term in self._terms:
            if term.is_positive():
                terms += "+"
            terms += term.__str__()

        return terms

    def __str__(self):
        if self._terms_string is None:
            self._terms_string = self._build_terms()

        return self._terms_string
