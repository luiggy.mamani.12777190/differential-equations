from sympy import latex

def promptSolve(equation: list):
    for term in equation:
        print(latex(term))

def promptProcedure(equation: list):
    print(equation[0])
    for term in range(1,len(equation)):
        print(latex(equation[term]))

