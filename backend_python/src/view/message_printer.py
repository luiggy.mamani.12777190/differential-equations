from colorama import Style
from model.error_management.alert import Alert
from sys import stderr


class Message_Printer:
    alert: Alert = None

    def __init__(self):
        self.alert = Alert.get_instance()
        self.print_err = stderr


    def show_message(self):
        if not self.alert.is_empty():
            self.print_err.write(self.alert.type.value + self.alert.message+"\n")
            self.alert.clear()
