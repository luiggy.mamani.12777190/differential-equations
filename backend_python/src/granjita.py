import click
import sympy
from model.differential_equations.equation import Equation
from model.handle.equation_builder import EquationBuilder

from model.logic.solvers.exact_equation.solver import Solver
from model.logic.solvers.exact_equation.verifier  import is_exact
from model.logic.solvers.exact_equation.reducer import Reducer
from model.logic.solvers.exact_equation.integration_factor_type import IFactorType
from model.logic.syntax_validator import Syntax_Validator
from view.message_printer import Message_Printer
from view.utils import promptSolve,promptProcedure


@click.group()
def cli():
    """Welcome to the BlueBell experience"""
    pass
@click.command()
@click.argument("equation")
# TODO: Add more solve types to the help
def solve(equation: str):
    """Solves the equation and shows the solution"""
    validator = Syntax_Validator()
    printer = Message_Printer()

    # SyntaxVerifier.check(equation)
    if validator.is_valid(equation):
        mathEquation: Equation = EquationBuilder().build_str_equation(equation)
        solver = Solver()
        process_solution: list = solver.solve(mathEquation)
        promptSolve(process_solution)
    else:
        printer.show_message()

cli.add_command(solve)

@click.command()
@click.argument("equation")
def verify(equation: str):
    """Verify if the equation can be solved by the method exact differential equation"""
    validator = Syntax_Validator()
    printer = Message_Printer()

    # SyntaxVerifier.check(equation)
    if validator.is_valid(equation):
        mathEquation: Equation = EquationBuilder().build_str_equation(equation)
        verification_result ,process_verification_result = is_exact(mathEquation)
        process_verification_result = [f"PROCESS STATUS : {verification_result}"]+process_verification_result
        promptProcedure(process_verification_result)
    else:
        printer.show_message()
cli.add_command(verify)


@click.command()
@click.argument("equation")
@click.option(
    "--formula",
    type=click.Choice(['m','n']),
    help="Defines the type of formula to be used to reduce the equation",
)
def reduce(equation: str, formula):
    """Reduce an equation so that it can be solved by the method of exact differential equations. """
    validator = Syntax_Validator()
    printer = Message_Printer()
    # SyntaxVerifier.check(equation)
    if validator.is_valid(equation):
        mathEquation: Equation = EquationBuilder().build_str_equation(equation)
        reduccion_procedure, equation_result = [], None
        reducer = Reducer()
        if formula== "m":
            equation_result, reduccion_procedure = reducer.reduce(mathEquation,IFactorType.CALCULATE_BY_M)
        else:
            equation_result, reduccion_procedure = reducer.reduce(mathEquation,IFactorType.CALCULATE_BY_N)
        reduccion_procedure = [str(equation_result)]+reduccion_procedure
        promptProcedure(reduccion_procedure)
    else:
        printer.show_message()
cli.add_command(reduce)
